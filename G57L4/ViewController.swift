//
//  ViewController.swift
//  G57L4
//
//  Created by Alex Pylypchuk on 10/5/17.
//  Copyright © 2017 Alex Pylypchuk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let inputText = inputTextField.text!
        if inputText.isEmpty {
            print("Please insert numbers to text field")
            return
        }
        let number = Int(Double(inputText)!)
        //TextFieldHandler.printHelloWord(printCount: number)
        //TextFieldHandler.printWeekDayName(dayNumber: number)
        //TextFieldHandler.printFibonachiNumbers(fNumber: number)
        //TextFieldHandler.convertInchToCentimeter(inch: number)
        //TextFieldHandler.song(numberCount: number)
        TextFieldHandler.wholeNumbers(inputNumber: number)
    }
}
