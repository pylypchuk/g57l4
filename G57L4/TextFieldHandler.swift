//
//  TextFieldHandler.swift
//  G57L4
//
//  Created by Alex Pylypchuk on 10/5/17.
//  Copyright © 2017 Alex Pylypchuk. All rights reserved.
//

import UIKit

class TextFieldHandler: NSObject {
    
    static func printHelloWord(printCount: Int = 1) {
        for _ in 0..<printCount {
            print("Hello world")
        }
    }
    
    static func printWeekDayName(dayNumber: Int) {
        switch dayNumber%7 {
        case 1:
            print("Mondey")
        case 2:
            print("Tuesday")
        case 3:
            print("Wednesday")
        case 4:
            print("Thursday")
        case 5:
            print("Friday")
        case 6:
            print("Saturday")
        case 7:
            print("Sunday")
        default:
            print("Sunday")
        }
    }
    
    static func printFibonachiNumbers(fNumber : Int) {
        var parentNumber = 0;
        var preParentNumber = 0;
        
        for index in 0..<fNumber {
            
            let fibonachiNumber = parentNumber + preParentNumber
            
            if (index == 0) {
                parentNumber = 1
            } else {
                preParentNumber = parentNumber;
                parentNumber = fibonachiNumber
            }
            
            print(fibonachiNumber)
        }
    }
    
    static func convertInchToCentimeter(inch : Int) {
        let inchInOneCentimeter = 2.54
        let result = inchInOneCentimeter * Double(inch)
        
        print(result)
    }
    
    static func song(numberCount: Int) {
        for index in 0..<numberCount {
            let count = numberCount - index
            print("\(count) bottles of beer on the wall.")
            print("\(count) bottles of beer")
            print("You take one down, pass it around")
        }
    }
    
    static func wholeNumbers(inputNumber: Int) {
        for index in 1..<100 {
            if(index%inputNumber == 0) {
                print(index)
            }
        }
    }

}
